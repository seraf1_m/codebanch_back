import express from "express";
import cors from 'cors'
import mongoose from "mongoose";
import checkAuth from "./utils/checkAuth.js";
import * as UserController from "./controllers/UserController.js";
import * as TaskController from "./controllers/TaskController.js";
import {loginValidation, registerValidation, taskValidation} from "./validation.js";
import handleValidationErrors from "./utils/handleValidationErrors.js";
import checkAdmin from "./utils/checkAdmin.js";

//НАСТРОЙКА СЕРВЕРА
mongoose.connect(process.env.DB_URI)
  .then(() => console.log('бд подключена'))
  .catch((e) => console.log(e))

const app = express()
app.use(express.json())
app.use(cors())
app.get('/', (req, res) => {
  res.send('Hello World!')
})

//РЕГИСТРАЦИЯ
app.get('/auth/me', checkAuth, UserController.getMe)
app.post('/auth/login', loginValidation, handleValidationErrors, UserController.login)
app.post('/auth/register', registerValidation, handleValidationErrors, UserController.register)

//ЗАДАЧИ
app.post('/tasks', checkAdmin, taskValidation, handleValidationErrors, TaskController.addTask)
app.get('/tasks', TaskController.getTasks)
app.get('/tasks/:id', TaskController.getTask)
app.delete('/tasks/:id', checkAdmin, TaskController.remove)
app.post('/tasks/:id', checkAuth, TaskController.passTask)
app.post('/tasks/check/:id', TaskController.checkTask)



//ПОРТ
app.listen(process.env.PORT || 5000, (err) => {
  if(err) {
    return console.log(err)
  }
  console.log('Сервер подключен')
})