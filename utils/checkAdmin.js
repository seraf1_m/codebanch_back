import jwt from "jsonwebtoken";
import User from "../models/User.js";

export default async(req, res, next) => {
  const token = (req.headers.authorization || '').replace(/Bearer\s?/, '');
  if (token) {
    try {
      const decoded = jwt.verify(token, 'secret123');
      req.userId = decoded._id;
      const user = await User.findById(decoded._id)
      if(user.isAdmin == true) {
        next()
      }
      else {
        return res.status(403).json({
          message: 'Нет доступа'
        })
      }
    } catch (e) {
      return res.status(403).json({
        message: 'Нет доступа'
      })
    }
  } else {
    return res.status(403).json({
      message: 'Нет доступа'
    })
  }
}