import Task from "../models/Task.js";
import TaskModel from "../models/Task.js";
import UserModel from "../models/User.js";
import User from "../models/User.js";

export const addTask = async (req, res) => {
  try {
    const doc = new TaskModel({
      name: req.body.name,
      description: req.body.description,
      tests: req.body.tests,
      speedTest: req.body.speedTest,
      defaultFunction: req.body.defaultFunction,
    });
    const task = await doc.save()
    res.json(task)
  } catch (err) {
    console.log(err)
    res.status(500).json({
      message: "Не удалось создать задачу",
    })
  }
}

export const getTasks = async (req, res) => {
  try {
    const tasks = await TaskModel.find().exec()
    res.json(tasks)
  } catch (err) {
    console.log(err)
    res.status(500).json({
      message: "Не удалось получить задачи",
    })
  }
}

export const getTask = async (req, res) => {
  try {
    const task = await TaskModel.findById(req.params.id)
    if(!task) {
      return res.status(404).json({
        message: "Задача не найдена"
      })
    }
    res.json(task)
  } catch (err) {
    console.log(err)
    res.status(500).json({
      message: "Не удалось получить задачи",
    })
  }
}


export const remove = async (req, res) => {
  try {
    const taskId = await req.params.id
    const task = await TaskModel.findById(taskId)
    if(!task) {
      return res.status(404).json({
        message: "Задача не найдена"
      })
    } else {
      await TaskModel.findByIdAndDelete(taskId)
      res.json({
        success: true
      })
    }
  } catch (e) {
    console.log(e)
    res.status(500).json({
      message: "Не удалось удалить задачу"
    })
  }
}

export const passTask = async (req, res) => {
  try {
    const taskId = await req.params.id
    const userId = await req.body.userId;
    const task = await TaskModel.findById(taskId)
    const user = await User.findById(userId)
    const ids = task.users.map(user => String(user.user._id))
    const {users, defaultFunction, tests, speedTest, ...taskData} = await task._doc
    console.log({...taskData})
    if(ids.includes(String(user._id))) {
      return res.json({
        message: "Вы уже прошли задачу"
      })
    }
    if(!task) {
      return res.status(500).json({
        message: "Задача не найдена"
      })
    } else {
      await TaskModel.findByIdAndUpdate(taskId, {
        $push: {
          users: {
            user: user,
            timeDecision: req.body.timeDecision,
            decision: req.body.decision
          }
        }
      })
      await UserModel.findByIdAndUpdate(req.body.userId, {
        $push: {
          completedTasks: {
            task: taskData,
            decision: req.body.decision
          }
        }
      })
      res.json({
        success: true
      })
    }
  } catch (e) {
    console.log(e)
    res.status(500).json({
      message: "Не удалось удалить задачу"
    })
  }
}



export const checkTask = async(req, res) => {
  try {
    let func;
    try {
      func = new Function(`return ${req.body.code}`)
    } catch(e) {
      return res.status(400).json({
        error: String(e)
      })
    }
    
    const task = await TaskModel.findById(req.params.id)
    let functionBody = func();
    try {
      functionBody(...task.tests[0].args)
    } catch(e) {
      return res.status(404).json({
        error: String(e)
      })
    }
    let result;
    const tests = task.tests.map(test => functionBody(...test.args) === test.expected)
    if( tests.includes(false) ) {
      return res.json({
        error: 'Задача решена неверно'
      })
    } else {
      let score = 0;
      let need = Date.now() + 1000;
      while (Date.now() < need) {
        functionBody(...task.speedTest);
        score++;
      }
      return res.json({
        message: 'Все тесты пройдены',
        score: score
      })
    }
  } catch (e){
    console.log(e)
    return res.json({
      message: e
    })
  }
}