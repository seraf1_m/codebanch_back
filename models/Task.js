import mongoose from "mongoose";


const TaskSchema = new mongoose.Schema({
  name: {
    type: String,
    required: true,
    unique: true
  },
  description: {
    type: String,
    required: true,
    unique: true
  },
  tests: {
    type: Array,
    required: true,
  },
  speedTest: {
    required: true,
    type: Array
  },
  users: {
    type: Array,
    user: {
      type: mongoose.Schema.Types.ObjectId,
      ref: "User",
      required: true,
      unique: true
    },
    timeDecision: Number,
    decision: String,
    defaultValue: []
  },
  defaultFunction: {
    type: String,
    required: true
  }

}, {
  timestamps: true,
})

export default mongoose.model('Task', TaskSchema)