import mongoose from "mongoose";

const UserSchema = new mongoose.Schema({
  login: {
    type: String,
    required: true,
    unique: true
  },
  completedTasks: {
    type: Array,
    task: {
      type: mongoose.Schema.Types.ObjectId,
      ref: "Task",
      required: true,
      unique: true
    },
    defaultValue: []
  },
  passwordHash: {
    type: String,
    required: true
  },
  avatarUrl: String,
  isAdmin: {
    type: Boolean,
    defaultValue: false
  }
}, {
  timestamps: true,

})

export default mongoose.model('User', UserSchema)