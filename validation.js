import { body } from 'express-validator'

export const registerValidation = [
  body('password', 'Пароль должен быть минимум 5 символов').isLength({ min: 5}),
  body('login', 'Логин должен быть минимум 3 символа').isLength({ min: 3}),
  body('avatarUrl', 'Неверная ссылка на аватарку').optional().isURL(),
]

export const loginValidation = [
  body('login', 'Неверный логин или пароль').isString().isLength({min: 3}),
  body('password', 'Неверный логин или пароль').isLength({ min: 6}),
]
export const taskValidation = [
  body('name', 'Введите название задачи').isString().isLength({min: 1}),
  body('description', 'Введите описание задачи').isString().isLength({ min: 5}),
  body('defaultFunction', 'Как должна выглядеть функция').isString().isLength({ min: 5}),
  body('tests', 'Введите аргументы для тестов и результат').isArray(),
  body('speedTest', 'Введите агрументы для максимальной нагрузки скрипта').isArray(),
]

